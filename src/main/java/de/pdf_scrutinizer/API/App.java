/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API;

import java.util.ArrayList;
import java.util.List;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.API.app.*;
import de.pdf_scrutinizer.utils.Reflect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;

public class App {
    private Log log = LogFactory.getLog(App.class);
    private final Scrutinizer scrutinizer;
    public final Doc doc;
    private plugIn plugin = new plugIn("EScript", 5.13);
    public plugIn[] plugIns = new plugIn[]{plugin, new plugIn(), new plugIn(), new plugIn()};

    public double viewerVersion = 8;
    public String viewerType = "Reader";
    public String platform = "WIN";

    public Media media = new Media();

    public App(Scrutinizer scrutinizer, Doc doc) {
        this.scrutinizer = scrutinizer;
        this.doc = doc;
    }

    public plugIn getPlugin() {
        return plugin;
    }

    public void setPlugin(plugIn p) {
        plugin = p;
    }

    private List<Timeout> timeouts = new ArrayList<Timeout>();

    public List<Timeout> getTimeouts() {
        return timeouts;
    }

    public Timeout setTimeOut(final String cExpr, final int nMilliseconds) {
        Reflect.getMethodName();

        scrutinizer.getStaticAnalysis().doit(cExpr);
        final Context cx = Context.getCurrentContext();
        final Scriptable scope = (Scriptable) cx.getThreadLocal("topscope");

        Runnable task = new Runnable() {
            @Override
            public void run() {
                Log log = LogFactory.getLog(Timeout.class);

                if (log.isDebugEnabled()) {
                    log.debug("starting timer task");

                    String tmp = cExpr.trim().replace("\\s", " ").replace("\r\n", " ").replace("\n", " ");
                    if (tmp.length() > 50) {
                        log.debug("code peek: \"" + tmp.substring(0, 50) + "...\"");
                    } else {
                        log.debug("code peek: \"" + tmp + "\"");
                    }
                }

                Context c = Context.enter();
                c.putThreadLocal("scrutinizer", scrutinizer);
                c.setOptimizationLevel(-1);
                c.evaluateString(scope, cExpr, "app.setTimeOut", 0, null);
                Context.exit();

                log.debug("finished timer task");
            }
        };

        Timeout timeout = new Timeout(task, nMilliseconds);
        timeouts.add(timeout);
        return timeout;
    }

    public Timeout setInterval(final String cExpr, final int nMilliseconds) {
        Reflect.getMethodName();

        scrutinizer.getStaticAnalysis().doit(cExpr);
        final Context cx = Context.getCurrentContext();
        final Scriptable scope = (Scriptable) cx.getThreadLocal("topscope");

        Runnable task = new Runnable() {
            @Override
            public void run() {
                Log log = LogFactory.getLog(Timeout.class);

                if (log.isDebugEnabled()) {
                    log.debug("starting timer task");

                    String tmp = cExpr.trim().replace("\\s", " ").replace("\r\n", " ").replace("\n", " ");
                    if (tmp.length() > 50) {
                        log.debug("code peek: \"" + tmp.substring(0, 50) + "...\"");
                    } else {
                        log.debug("code peek: \"" + tmp + "\"");
                    }
                }

                Context c = Context.enter();
                c.putThreadLocal("scrutinizer", scrutinizer);
                c.setOptimizationLevel(-1);
                c.evaluateString(scope, cExpr, "app.setInterval", 0, null);
                Context.exit();

                log.debug("finished timer task");
            }
        };

        Timeout timeout = new Timeout(task, nMilliseconds, true);
        timeouts.add(timeout);
        return timeout;
    }

    public void clearInterval(Object timeout) {
        Reflect.getMethodName();

        if (timeout instanceof Timeout) {
            ((Timeout) timeout).stop();
        } else {
            log.warn("non-timeout object passed to clearInterval()?!");
        }
    }

    public void clearTimeOut(Object timeout) {
        Reflect.getMethodName();

        if (timeout instanceof Timeout) {
            ((Timeout) timeout).stop();
        } else {
            log.warn("non-timeout object passed to clearTimeOut()?!");
        }
    }

    public int alert(String str) {
        Reflect.getMethodName();

        System.out.println(str);
        return 1;
    }

    public int alert(NativeObject o) {
        Reflect.getMethodName();
        return 1;
    }

    public int alert(String a, int b, int c) {
        Reflect.getMethodName();
        return 1;
    }

    public void findComponent(NativeObject o) {
        Reflect.getMethodName();

        String cType = "";
        String cName = "";
        String cVer = "";
        if (o.containsKey("cType")) {
            cType = (String) o.get("cType");
        }
        if (o.containsKey("cName")) {
            cName = (String) o.get("cName");
        }
        if (o.containsKey("cVer")) {
            cVer = (String) o.get("cVer");
        }

        log.info(String.format("this is a stub: app.findComponent({cType:\"%s\", cName:\"%s\", cVer:\"%s\"})", cType, cName, cVer));
    }
}