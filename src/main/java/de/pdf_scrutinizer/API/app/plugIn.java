/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.API.app;

public class plugIn {
    public plugIn() {

    }

    public plugIn(String name_) {
        name = name_;
    }

    public plugIn(String name_, double version_) {
        name = name_;
        version = version_;
    }

    public String name = "";

    /* all properties read only */
    public double version = 5.11;
    public final boolean certified = true;
    public final boolean loaded = true;
    public final String path = "";
}