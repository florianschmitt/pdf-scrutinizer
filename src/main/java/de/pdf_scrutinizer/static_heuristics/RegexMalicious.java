/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.static_heuristics;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.LogFactory;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.utils.Input;

public class RegexMalicious extends StaticHeuristic {
    private final List<Pattern> patterns;

    public RegexMalicious(Scrutinizer scrutinizer) {
        super(scrutinizer);
        log = LogFactory.getLog(RegexMalicious.class);
        patterns = getPatternsFromFile(Input.REGEX_MALICIOUS);
    }

    @Override
    public void run(String code) {
        for (Pattern x : patterns) {
            Matcher matcher = x.matcher(code);
            if (matcher.find()) {
                log.info(String.format("\"%s\" found", x));
                scrutinizer.getAnalysisResult().heuristicFulfilled("RegexMalicious");
            }
        }
    }
}