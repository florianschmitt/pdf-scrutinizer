/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.*;
import com.mongodb.util.JSON;
import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.AnalysisResult;
import de.pdf_scrutinizer.dynamic_heuristics.ShellcodeTester;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.HexDump;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.BSONObject;

import java.io.*;
import java.net.UnknownHostException;
import java.util.List;

public class OutputToMongoDB extends OutputNull {
    private static Log log = LogFactory.getLog(OutputToMongoDB.class);
    private final Scrutinizer scrutinizer;

    private DB db;
    private final BasicDBObject urlIdObj;

    public OutputToMongoDB(Scrutinizer scrutinizer, String urlId) {
        this.scrutinizer = scrutinizer;
        urlIdObj = new BasicDBObject("url_id", urlId);
        MongoClient mongoClient;
        try {
            mongoClient = new MongoClient("localhost");
        } catch (UnknownHostException e) {
            log.error(e);
            return;
        }

        db = mongoClient.getDB("scrutinizer");

        setMongoDbState("unfinished");
    }

    private void setMongoDbState(String state) {
        DBCollection stateDb = db.getCollection("state");
        DBObject obj = stateDb.findOne(urlIdObj);

        if (obj != null) {
            obj.put("state", state);
            stateDb.save(obj);
        } else {
            BasicDBObject stateObj = (BasicDBObject)urlIdObj.copy();
            stateObj.append("state", state);
            stateDb.insert(stateObj);
        }
    }

    @Override
    public void eval(String x) {
        DBCollection collection = db.getCollection("eval");
        DBObject b = collection.findOne(urlIdObj);

        if (b != null) {
            collection.update(b, new BasicDBObject("$push", new BasicDBObject("evalList", x)));
        } else {
            BasicDBObject obj = (BasicDBObject) urlIdObj.copy();
            BasicDBList evalList = new BasicDBList();
            evalList.add(x);
            obj.append("evalList", evalList);
            collection.insert(obj);
        }
    }

    @Override
    public void functionCalls(String x) {
    }

    private int embedStage = 1;

    @Override
    public void saveAnalysisResult(AnalysisResult result) {
        DBCollection collection = db.getCollection("analysisResult");
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            setMongoDbState("finished (error)");
            return;
        }

        DBObject obj = (DBObject) JSON.parse(json);
        obj.putAll((BSONObject)urlIdObj);
        collection.save(obj);
        setMongoDbState("finished");
    }

    @Override
    public void saveEmbedFile(String[] str) {

    }

    @Override
    public void saveExtractedcode(List<String> codes) {

    }

    //Destructor
    @Override
    protected void finalize() throws Throwable {
        db.getMongo().close();
    }

    private int sccount = 0;

    @Override
    public void saveShellcode(String sc, String stdout) {

    }

    @Override
    public void prettyPrint(String str) {

    }

    @Override
    public void dynamicICode(String str) {

    }

    @Override
    public void treeICode(String str) {

    }

    @Override
    public PrintStream getStaticICodePrintStream() {
        return null;
    }

    @Override
    public void setSaveDynamicICode(boolean saveDynamicICode) {

    }

    @Override
    public boolean getSaveDynamicICode() {
        return false;
    }

    @Override
    public void setShowHexdump(boolean showHexdump) {

    }
}
