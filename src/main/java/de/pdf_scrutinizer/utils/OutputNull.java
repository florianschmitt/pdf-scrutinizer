/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.utils;

import de.pdf_scrutinizer.data.AnalysisResult;

import java.io.PrintStream;
import java.util.List;

public class OutputNull implements Output {
    @Override
    public void saveAnalysisResult(AnalysisResult result) {

    }

    @Override
    public void saveEmbedFile(String[] str) {

    }

    @Override
    public void eval(String x) {

    }

    @Override
    public void functionCalls(String x) {

    }

    @Override
    public void saveExtractedcode(List<String> codes) {

    }

    @Override
    public void prettyPrint(String str) {

    }

    @Override
    public void saveShellcode(String sc, String stdout) {

    }

    @Override
    public void dynamicICode(String str) {

    }

    @Override
    public PrintStream getStaticICodePrintStream() {
        return null;
    }

    @Override
    public void treeICode(String str) {

    }

    @Override
    public void setSaveDynamicICode(boolean saveDynamicICode) {

    }

    @Override
    public void setShowHexdump(boolean showHexdump) {

    }

    @Override
    public boolean getSaveDynamicICode() {
        return false;
    }
}