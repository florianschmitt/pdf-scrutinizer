/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.dynamic_heuristics;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.NativeArray;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.utils.Hashes;

public class DynamicHeuristics {
    private Log log = LogFactory.getLog(DynamicHeuristics.class);

    private static final int STRING_THRESHOLD = 100000;

    private final Scrutinizer scrutinizer;
    private final Set<String> shellcodeTestedStrings = Collections.synchronizedSet(new HashSet<String>());
    private final Set<String> arrayStrings = Collections.synchronizedSet(new HashSet<String>());
    private ShellcodeTester shellcodeTester;
    private boolean stringlength_informed = false;
    private boolean heapspray_informed = false;

    public DynamicHeuristics(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    public void setShellcodeTester(ShellcodeTester shellcodeTester) {
        this.shellcodeTester = shellcodeTester;
    }

    public ShellcodeTester getShellcodeTester() {
        return this.shellcodeTester;
    }

    private boolean testLength(Object object) {
        if (object instanceof String) {
            String string = (String) object;
            if (string.length() > STRING_THRESHOLD) {
                if (!stringlength_informed) {
                    scrutinizer.getAnalysisResult().heuristicFulfilled("StringLengthTester");
                    log.info("This is a very long string: " + string.length());
                    stringlength_informed = true;
                }

                return true;
            }
        }

        return false;
    }

    public boolean set_var(Object object) {
        boolean result = testLength(object);

        if (object instanceof String) {
            possibleShellcode((String) object);
        }

        return result;
    }

    public boolean setelem(Object lhs, Object rhs) {
        if (rhs instanceof String && lhs instanceof NativeArray) {
            String heapsprayblock = (String) rhs;
            int length = ((heapsprayblock.length()) / 512) + 1;  //java char length 16bit

            if (length > 50) { //50KB
                String heapsprayblockhash = Hashes.getHashOfString(heapsprayblock);
                if (!arrayStrings.contains(heapsprayblockhash)) {
                    log.debug("adding " + length + " KB element to an array");
                    heapspray_informed = false;

                    //They will not be detected because they are too long... libemu is too slow
                    possibleShellcode(heapsprayblock);

                    if (!arrayStrings.contains(heapsprayblockhash)) {
                        arrayStrings.add(heapsprayblockhash);
                    }
                } else {
                    if (!heapspray_informed) {
                        scrutinizer.getAnalysisResult().heuristicFulfilled("HeapSprayDetector");
                        log.info("heap spraying detected");
                        heapspray_informed = true;
                    }

                    return true;
                }
            }
        }
        return false;
    }

    public void reg_str_1(String string) {
        possibleShellcode(string);
    }

    public void setname(Object object) {
        if (object instanceof String) {
            possibleShellcode((String) object);
        }
    }

    private void addToTestedStrings(String hash) {
        if (!shellcodeTestedStrings.contains(hash)) {
            shellcodeTestedStrings.add(hash);
        }
    }

    public void possibleShellcode(String str) {
        if (shellcodeTester != null) {
            if (str.length() > 50 && str.length() < 1500) {   // this opts out heapspray blocks because its too slow...
                String hash = Hashes.getHashOfString(str);
                if (!shellcodeTestedStrings.contains(hash)) {
                    if (str.contains("var") || str.contains("function") || str.contains("if")) {
                        addToTestedStrings(hash);
                        return; // JS-Code
                    }

                    if (str.contains("%u")) {
                        addToTestedStrings(hash);
                        return; //unescaped unicode string
                    } //or unescape now?

                    boolean unicode = false;
                    for (int i = 0; i < str.length(); i++) {
                        if ((byte) (str.charAt(i) >> 8) != 0) { //Non-unicode strings: all high-bytes zero
                            unicode = true;
                        }
                    }

                    if (!unicode) {
                        addToTestedStrings(hash);
                        return;
                    }

                    boolean zero = fourCharXOR(str);

                    if (zero) {
                        addToTestedStrings(hash);
                        return;
                    }

	                /*
                     * Problem:
	                 * if the code builds a string, every part string is tested. (between lower and upper bound)
	                 * 
	                 * e.g.
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b0
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b0c
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b0c%
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b0c%u
	                 * %ueb55%u336e%u64c0%u408b%u8530%u78c0%u560d%u408b%u8b0c%u1
	                 * 
	                 * Solution for now: kick out strings with "%u", these will be escaped later.
	                 * 
	                 */

                    log.info("test string for shellcode.");
                    scrutinizer.getBenchmark().shellcodeStart();
                    shellcodeTester.testShellcode(str);
                    scrutinizer.getBenchmark().shellcodeStop();
                    addToTestedStrings(hash); // does that make sense?
                }
            }
        }
    }

    private static boolean fourCharXOR(String str) {
        //check if the string is just junk-code like
        //0x90ABCDEF90ABCDEF... or 0x909090909090909090...
        //4 java chars = 8 byte
        char[] chars = str.toCharArray();
        char c1 = chars[0]; //xor char1
        char c2 = chars[1]; //xor char2
        char c3 = chars[2]; //xor char3
        char c4 = chars[3]; //xor char4    
        boolean zero = true;

        for (int i = 3; i < chars.length; i += 4) {
            if ((chars[i - 3] ^ c1) != 0 || (chars[i - 2] ^ c2) != 0 || (chars[i - 1] ^ c3) != 0 || (chars[i] ^ c4) != 0) {
                zero = false;
                break;
            }
        }
        return zero;
    }
}