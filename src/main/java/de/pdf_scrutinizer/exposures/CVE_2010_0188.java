/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.exposures;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDXFA;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.Vulnerability;
import de.pdf_scrutinizer.document.DocumentAdapter;

public class CVE_2010_0188 implements DocumentExposure {
    private Log log = LogFactory.getLog(CVE_2010_0188.class);
    private final Scrutinizer scrutinizer;

    public CVE_2010_0188(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    public void run(PDDocument document) {
        PDDocumentCatalog catalog = document.getDocumentCatalog();
        PDAcroForm af = catalog.getAcroForm();

        if (af != null) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            Document doc = null;
            String xml = null;
            boolean parsingerror = false;
            try {
                db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                PDXFA xfa = af.getXFA();
                if (xfa == null)
                    return;
                xml = DocumentAdapter.xmlFromXFA(xfa);
                is.setCharacterStream(new StringReader(xml));
                doc = db.parse(is);
            } catch (ParserConfigurationException e) {
                log.warn("", e);
                parsingerror = true;
            } catch (SAXException e) {
                log.warn(e.getMessage());
                parsingerror = true;
            } catch (IOException e) {
                log.warn("", e);
                parsingerror = true;
            }

            if (!parsingerror) {
                NodeList l = doc.getElementsByTagName("ImageField1");
                Element e = (Element) l.item(0); //<ImageField1 xfa:contentType="image/tif"

                if (e != null) {
                    checkString(DocumentAdapter.getCharacterDataFromElement(e));
                }
            } else {
                /*
                 * I have seen samples where the XML stopped right after the ImageField1-element content.
                 * In this case we can't parse the XML with org.w3c.dom.Document class. Thats why we search here
                 * with a regular expression.
                 */
                Pattern p = Pattern.compile("xfa:contentType=\"image/tif\"\\shref=\"\"\\s?>([^<]*)<?", Pattern.DOTALL);

                Matcher m = p.matcher(xml);

                if (m.find()) {
                    checkString(m.group(1));
                }
                
                /*
                 * the same can happen to XML's with <script> tags
                 */
                p = Pattern.compile("<script\\s[\\w\\s=\\\"/-]*>([^<]*)<?", Pattern.DOTALL);

                m = p.matcher(xml);
                if (m.find()) { //FIXME: what if more than one <script> ?
                    String tmp = m.group(1);
                    tmp = StringEscapeUtils.unescapeXml(tmp);

                    //FOR NOW: search statically for BASE64-TIFF strings
                    //TODO: implement Imagefield class that checks dynamically

                    Pattern p2 = Pattern.compile("\"([^\"]*)\"", Pattern.DOTALL);
                    Matcher m2 = p2.matcher(tmp);

                    if (m2.find()) {
                        for (int i = 1; i <= m2.groupCount(); i++) {
                            checkString(m2.group(i));
                        }
                    }
                }
            }
        }
    }

    public void checkString(String string) {
        String str = string.trim().replace("\\s", "").replace("\r\n", "").replace("\n", "");
        int remain = str.length() % 4;
        String fillstr = "";

        //TODO: do we really need this? 
        //or does Base64.decodeBase64 fill by its own?
        int fill = 4 - remain;
        if (fill == 1) {
            fillstr = "=";
        } else if (fill == 2) {
            fillstr = "==";
        } else if (fill == 3) {
            fillstr = "==="; //FIXME: specification does not allow this
        }

        byte[] bytes = Base64.decodeBase64(str.concat(fillstr));

        int tiff_offset = 0;
        tiff_offset |= bytes[7] << 24;
        tiff_offset |= bytes[6] << 16;
        tiff_offset |= bytes[5] << 8;
        tiff_offset |= bytes[4];

        if (bytes[tiff_offset] == 0x7
                && bytes[tiff_offset + 1] == 0x0
                && bytes[tiff_offset + 2] == 0x0
                && bytes[tiff_offset + 3] == 0x1) {
            Vulnerability v = new Vulnerability("CVE-2010-0188");
            v.description = "tiff integer overflow";
            v.references.add("http://blog.fortinet.com/cve-2010-0188-exploit-in-the-wild/");
            scrutinizer.getAnalysisResult().vulnerabilityUsed(v);
        }
    }
}
