/*
 * PDF Scrutinizer, a library for detecting and analyzing malicious PDF documents.
 * Copyright 2013  Florian Schmitt <florian@florianschmitt.de>, Fraunhofer FKIE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.pdf_scrutinizer.exposures;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocument;

import de.pdf_scrutinizer.Scrutinizer;
import de.pdf_scrutinizer.data.Vulnerability;

public class CVE_2009_0658 implements DocumentExposure {
    private Log log = LogFactory.getLog(CVE_2009_0658.class);

    private final Scrutinizer scrutinizer;

    public CVE_2009_0658(Scrutinizer scrutinizer) {
        this.scrutinizer = scrutinizer;
    }

    @Override
    public void run(PDDocument document) {
        List<COSObject> xobjects = null;

        try {
            xobjects = document.getDocument().getObjectsByType(COSName.XOBJECT);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }

        if (xobjects.size() > 0) {
            for (COSObject a : xobjects) {
                COSBase b = a.getObject();
                if (b instanceof COSStream) {
                    COSStream c = (COSStream) b;
                    COSBase d = c.getDictionaryObject(COSName.FILTER);

                    if (d == null)
                        return;

                    if (d.equals(COSName.JBIG2_DECODE)) {
                        InputStream e = null;
                        try {
                            e = c.getFilteredStream();
                            int headerflagbyte = -1;
                            for (int i = 0; i < 5; i++) {
                                headerflagbyte = e.read();
                            }

                            if ((headerflagbyte & 0x40) != 0) {
                                //large page association size bit is set
                                int segmentpageassociation = 0;

                                e.read(); //skip one byte

                                for (int i = 3; i > -1; i--) {
                                    segmentpageassociation |= (e.read() << (i * 8));
                                }

                                //FIXME:
                                // 
                                // thats just the PoC value.
                                // it could have different values!
                                //
                                if (segmentpageassociation == 0x333333) {
                                    Vulnerability v = new Vulnerability("CVE-2009-0658");
                                    v.description = "JBIG2 overflow";
                                    v.references.add("http://www.secureworks.com/research/blog/research/20947/");
                                    scrutinizer.getAnalysisResult().vulnerabilityUsed(v);
                                }
                            }
                        } catch (IOException ex) {
                            log.error(ex.getMessage(), ex);
                        } finally {
                            try {
                                e.close();
                            } catch (IOException ex) {
                                log.error(ex.getMessage(), ex);
                            }
                        }
                    }
                }
            }
        }
    }
}
